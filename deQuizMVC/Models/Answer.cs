﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deQuizMVC.Models
{
    public class Answer
    {
        public int QuestionNumber { get; set; } // The number of the question the answer is for.
        public int AnswerIndex { get; set; } // The index of the given answer.
        public bool? RightAnswer { get; set; } // Whether the given answer is right or not.
        public double AnswerTime { get; set; } // The amount of time in seconds that passed before the answer was given.
    }
}