﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deQuizMVC.Models
{
    public class QuizPlayer
    {
        public int Position { get; set; }
        public bool IsPlaying { get; set; }
        public List<Answer> Answers;

        public QuizPlayer()
        {
            Answers = new List<Answer>();
        }

    }
}