﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace deQuizMVC.Models
{
    public class LineOfDeath
    {
        public int Position { get; set; } // The position of the deathline

        private const int steps = 2; // The amount of steps the deathline moves after every question.

        public void Move()
        {
            Position += steps;
        }
    }
}